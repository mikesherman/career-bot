module.exports = {
  greet: require('./greet'),
  resume: require('./resume'),
  add: require('./add')
};
